Prédiction d'emojis dans des tweets en anglais et en espagnol

Tweets en anglais (500K) et en espagnol (100K) contenant chacun un unique emoji parmi les 20 plus fréquents dans la langue du tweet.
-> Problème de classification de séquences multi-classes.
Avec un gros jeu de données.
De ce que j'ai compris, on ne nous demande pas de prédire la langue du tweet. Quoique, ça pourrait être intéressant d'essayer de le faire (et si ça se trouve c'est trivial)

Représentation ? Vecteurs (pour Naive Bayes, Decision trees ...) ou conserver sequence information
Et pour les vecteurs, vecteurs de mots ou vecteurs 0/1 sur les mots les plus fréquents/significatifs ?

D'après les slides, 2 important choices: instance space/representation et hypothesis space
Pour la représentation d'un dataset de séquences:
-> énumération: uniquement pour de tous petits jeux de données. Ce n'est pas le cas ici, il y a 600K tweets en tout
-> caractérisation "mathématique" (propriétés des séquences): euuuh ???
-> grammaires
-> modèles stochastiques
-> SVM based on string kernels
-> Recurrent neural networks
---> à priori vu qu'on veut *classifier* nos séquences, il faut aller vers SVM based on string kernels ou recurrent neural networks.
--> donc du coup on ne peut pas jouer avec les grammaires pour ce que l'on veut faire ?
--> Si ! On dirait qu'on peut apprendre des automates et des grammaires pour classifier des séquences
--> est-ce que c'est bon aussi quand il y a plus de deux classes ? Intuitivement, je dirais qu'il "suffit" d'avoir 20 automates \_^.^_/
    et alors on se servirait des 19 autres emojis pour avoir plein d'exemples négatifs. C'est ok ?

Donc comme approches on a:
- Approches ML "classiques", données représentées sous forme de vecteurs -> vecteurs de mots ou vecteurs 0/1 sur les mots les plus fréquents ?
  - Naive Bayes -> le truc très simple mais qui fonctionne souvent très bien !! Peut être notre "baseline".
  - Arbres de décision
  - Réseaux de neurones mais sur des vecteurs ? (feedfront ? perceptron ?)
  - Random forests ?? D'autres trucs ???
- Classification de séquences en gardant l'information séquentielle
  - SVM avec string kernels -> je crois qu'on n'a pas vu les string kernels en cours, mais il y a une page Wikipédia lol
  - Réseaux de neurones récurrents/feedback (façon très naturelle de modéliser des données séquentielles)
  - Apprentissage d'automates/grammaires décrivant le langage des tweets qui vont contenir un certain emoji -> je prédis des résultats pourraves lol. Mais c'est intéressant d'essayer une approche différente.

Il faut essayer au moins trois approches.
Il faut comparer et étudier l'influence des différents choix qu'on a faits avec ces différentes méthodes.
Discuter les choix en terme de représentation, d'approche et de paramètres.
Il faut aussi étudier un peu plus l'influence d'un aspect de notre choix (bruit, paramètre, nombre d'exemples, impact de la représentation ...)
