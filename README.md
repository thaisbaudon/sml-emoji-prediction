# Projet de SML : prédiction d'emojis
Auteurs : Thaïs Baudon et Margot Masson

Vous trouverez ici les notebooks de Margot Masson et Thaïs Baudon pour le projet "prédiction d'emoji" dans le cadre du cours de SML du master SIF. Ces notebooks font office de rapport.

Vous trouverez également les données utilisées pour le projet (dossier data) ainsi que les slides de présentation du projet au format pdf.